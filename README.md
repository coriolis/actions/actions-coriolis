# actions-coriolis

Ce projet concerne les actions à traiter qui ne concerne pas de projets particuliers.

Actions liées aux serveurs / librairies / compilation / espace disque par exemple


# URLs
```
https://gitlab.ifremer.fr/coriolis/actions-argo/actions-coriolis.git
Ressources matériels: https://dev-ops.gitlab-pages.ifremer.fr/documentation/service_datasheet/information_systems/coriolis/resources/
```

